// Copyright 2019 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const (
	guiDescription = `redctl gui provides a graphical interface which can be used to create guest virtual machines.`
)

var (
	rootCmd = &cobra.Command{
		Use:           "redctl-gui",
		Short:         "redctl GUI application",
		Long:          guiDescription,
		SilenceUsage:  true,
		SilenceErrors: true,
		PreRunE: func(cmd *cobra.Command, args []string) error {
			if err := viper.BindPFlags(cmd.Flags()); err != nil {
				return err
			}

			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return Start(viper.GetString("redctl-address"), viper.GetString("icon"))
		},
	}
)

func main() {
	rootCmd.PersistentFlags().String("redctl-address", "unix:///var/run/redctld/redctl.socket", "redctl server address (unix:///path or ip:port)")
	rootCmd.PersistentFlags().String("icon", "", "Path to desktop icon for redctl GUI")

	if err := rootCmd.Execute(); err != nil {
		log.Println(err)
		os.Exit(1)
	}
}
