// Copyright 2019 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"path/filepath"
	"strconv"
	"time"

	"github.com/Equanox/gotron"
	"github.com/pkg/errors"

	"gitlab.com/redfield/redctl/api"
)

// waitForJSReady handles 'ready' event, waiting for JS to connect via WebSocket
func (ui *gui) waitForJSReady() {
	ready := make(chan bool)

	ui.window.On(&gotron.Event{Event: "ready"}, func([]byte) {
		ready <- true
	})

	<-ready
}

// registerDoneEventHandler handles "done" event, closes Electron window
func (ui *gui) registerDoneEventHandler() {
	ui.window.On(&gotron.Event{Event: "done"}, func([]byte) {
		ui.window.Close()
	})
}

// sendBackendsEvent sends a list of available VM network backends to the JS application
func (ui *gui) sendBackendsEvent() error {
	doms, err := ui.rc.DomainFindAll()
	if err != nil {
		err = errors.Wrap(err, "failed to find available network backends")
		ui.logErrorWithNotify(err)

		return err
	}

	backends := make([]string, 0)

	for _, dom := range doms {
		// TODO: We should eventually stop using the assumption that
		// pv domain == service domain == network backend.
		if dom.Config.Type == "pv" || dom.IsNetworkBackend {
			backends = append(backends, dom.Config.Name)
		}
	}

	if err = ui.sendMessage("backends", backends); err != nil {
		return err
	}

	return nil
}

// sendImagesEvent sends a list of available VM disk images to the JS application
func (ui *gui) sendImagesEvent() error {
	imgs, err := ui.rc.ImageFindAll()
	if err != nil {
		err = errors.Wrap(err, "failed to get available images")
		ui.logErrorWithNotify(err)

		return err
	}

	imgNames := make([]string, 0)

	for _, img := range imgs {
		if img.Format == api.ImageFormat_QCOW2 {
			imgNames = append(imgNames, img.Name)
		}
	}

	if err = ui.sendMessage("images", imgNames); err != nil {
		return err
	}

	return nil
}

// sendGraphicsEvent sends a list of VM desktop icons to the JS application
func (ui *gui) sendGraphicsEvent() error {
	graphics, err := ui.rc.GraphicFindAllDesktopIcons()
	if err != nil {
		err = errors.Wrap(err, "failed to get available desktop icons")
		ui.logErrorWithNotify(err)

		return err
	}

	paths := make([]string, 0)

	for _, graphic := range graphics {
		paths = append(paths, graphic.GetPath())
	}

	if err = ui.sendMessage("icons", paths); err != nil {
		return err
	}

	return nil
}

// registerVMCreateEventHandler handles "create" event, attempts to instantiate VM based upon user configuration
func (ui *gui) registerVMCreateEventHandler() {
	type eventCreateMessage struct {
		*gotron.Event
		Data struct {
			Name        string
			DesktopIcon string
		}
	}

	ui.window.On(&gotron.Event{Event: "create"}, func(j []byte) {
		var msg eventCreateMessage

		err := json.Unmarshal(j, &msg)
		if err != nil {
			err = errors.Wrap(err, "VM configuration is invalid")
			ui.logErrorWithNotify(err)

			return
		}

		if d, _ := ui.rc.DomainFindByUUIDThenName("", msg.Data.Name); d != nil {
			ui.logErrorWithNotify(errors.Errorf("VM name '%s' is already in use", msg.Data.Name))

			return
		}

		if ui.domain == nil {
			ui.domain = api.NewUserDomain(msg.Data.Name)
		} else {
			ui.domain.Config.Name = msg.Data.Name
		}

		ui.domain.DesktopIcon = filepath.Base(msg.Data.DesktopIcon)

		if err = ui.sendMessage("create", true); err != nil {
			log.Println(err)
		}
	})
}

// registerImageCreateEventHandler handles "image" event, creates a new VM disk image
func (ui *gui) registerImageCreateEventHandler() {
	type eventImageCreateMessage struct {
		*gotron.Event
		Data struct {
			Size         int
			InstallMedia string
		}
	}

	ui.window.On(&gotron.Event{Event: "image"}, func(j []byte) {
		var msg eventImageCreateMessage

		err := json.Unmarshal(j, &msg)
		if err != nil {
			err = errors.Wrap(err, "failed to parse image configuration")
			ui.logErrorWithNotify(err)

			return
		}

		// Convert UI-provided size in GB to bytes
		imgSize := int64(msg.Data.Size * 1e+9)
		imgName := fmt.Sprintf("%s.qcow2", ui.domain.Config.Name)

		img, err := ui.rc.ImageCreate(imgName, imgSize)
		if err != nil {
			err = errors.Wrap(err, "failed to create disk image")
			ui.logErrorWithNotify(err)

			return
		}

		ui.domain.AddDisk(api.NewDomainDisk(img.GetPath(), "qcow2"))

		if im := msg.Data.InstallMedia; im != "" {
			// XXX: Unfortunately this is the only way to change the context for
			// this call with the redctl client.
			//
			// This is necessary because we expect the copy to take a bit...
			ui.rc.RemoveTimeout()
			defer func() {
				ui.rc = ui.rc.WithTimeout((10 * time.Second).String())
			}()

			// Make a copy of the installation media to the image repository.
			iso, err := ui.rc.ImageCopy(im, filepath.Base(im))
			if err != nil {
				err = errors.Wrap(err, "failed to copy installation media")
				ui.logErrorWithNotify(err)

				return
			}

			cdrom := api.NewDomainDisk(iso.GetPath(), "raw")

			cdrom.Devtype = "cdrom"
			cdrom.Vdev = "hdc"

			ui.domain.AddDisk(cdrom)
		}

		if err = ui.sendMessage("image", img.GetPath()); err != nil {
			log.Println(err)
		}
	})
}

// registerVMUpdateEventHandler handles "update" event, attempts to update VM hardware and disk information
func (ui *gui) registerVMUpdateEventHandler() {
	type eventUpdateMessage struct {
		*gotron.Event
		Data struct {
			VCPUs    string
			Memory   int
			Backend  string
			Bridge   string
			DiskPath string
		}
	}

	ui.window.On(&gotron.Event{Event: "update"}, func(j []byte) {
		var msg eventUpdateMessage

		err := json.Unmarshal(j, &msg)
		if err != nil {
			err = errors.Wrap(err, "VM configuration is invalid")
			ui.logErrorWithNotify(err)

			return
		}

		if ui.domain, err = ui.rc.DomainCreate(ui.domain); err != nil {
			err = errors.Wrap(err, "unable to create VM")
			ui.logErrorWithNotify(err)

			return
		}

		ui.domain.Config.Vcpus = msg.Data.VCPUs
		ui.domain.Config.Memory = strconv.Itoa(msg.Data.Memory)
		ui.domain.AddNetwork(api.NewDomainNetwork(msg.Data.Backend, msg.Data.Bridge, ""))

		if msg.Data.DiskPath != "" {
			img, err := ui.rc.ImageFind(msg.Data.DiskPath)
			if err != nil {
				err = errors.Wrapf(err, "could not find image at %s", msg.Data.DiskPath)
				ui.logErrorWithNotify(err)

				return
			}

			ui.domain.AddDisk(api.NewDomainDisk(img.GetPath(), "qcow2"))
		}

		if err = ui.rc.DomainUpdate(ui.domain); err != nil {
			err = errors.Wrap(err, "failed to apply changes to VM configuration")
			ui.logErrorWithNotify(err)

			return
		}

		if err = ui.sendMessage("update", true); err != nil {
			log.Println(err)
		}
	})
}
