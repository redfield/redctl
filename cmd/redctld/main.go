// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"log"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/redfield/redctl/internal/server"
)

var rootCmd = &cobra.Command{
	Use:   "redctld",
	Short: "",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		if viper.GetString("tls-certificate") == "" {
			log.Fatalf("tls enabled - must specify --tls-certifate=cert")
		}

		if viper.GetString("tls-key") == "" {
			log.Fatalf("tls enabled - must specify --tls-key=key_file")
		}

		var opts server.RedctlServerOptions
		opts.ServerURL = viper.GetString("server-url")
		opts.ServerRootPath = viper.GetString("server-root-path")
		opts.TLSCertFile = viper.GetString("tls-certificate")
		opts.TLSKeyFile = viper.GetString("tls-key")
		opts.DomainStartTimeout = time.Second * time.Duration(viper.GetUint("domain-start-timeout"))

		log.Printf("opts: %+v\n", opts)

		err := server.Serve(opts)
		if err != nil {
			log.Fatalf("failed to initialize server: %v", err)
		}
	},
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.Flags().String("server-url", "unix:///var/run/redctld/redctl.socket", "Server Address (unix:///path or tcp://host:port)")
	rootCmd.Flags().String("server-root-path", "/", "Server root path")
	rootCmd.Flags().String("tls-certificate", "/storage/services/redctld/server-certificate.pem", "Server Certificate")
	rootCmd.Flags().String("tls-key", "/storage/services/redctl/server-key.pem", "Server Key")
	rootCmd.Flags().Uint("domain-start-timeout", 3, "Time in seconds to wait for a dependent domain to hit the run state before giving up.")

	// bind all pflags to configuration file
	err := viper.BindPFlags(rootCmd.Flags())
	if err != nil {
		panic("failed to init viper.BindPFlags()")
	}
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	viper.SetConfigFile("/storage/services/redctld/config.yaml")
	viper.SetEnvPrefix("REDCTLD")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		log.Println("Using config file:", viper.ConfigFileUsed())
	} else {
		log.Println("Failed to read config file:", err.Error())
	}
}

func main() {
	err := rootCmd.Execute()
	if err != nil {
		log.Println("exiting with error:", err)
	}
}
