// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"syscall"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"golang.org/x/crypto/ssh/terminal"
)

// luksCmd represents the luks command
var (
	luksCmd = &cobra.Command{
		Use:   "luks",
		Short: "Manage LUKS software disk encryption",
		Long:  `Assign, query, and remove key slots`,
	}
)

func askExistingPass() []byte {
	fmt.Printf("Enter existing passphrase: ")
	key, err := terminal.ReadPassword(syscall.Stdin)
	fmt.Println("")

	if err != nil {
		log.Fatalf("ReadPassword err: %v\n", err)
		return nil
	}

	return key
}

func askNewPass() []byte {
	fmt.Printf("Enter new passphrase: ")
	key, err := terminal.ReadPassword(syscall.Stdin)
	fmt.Println("")

	if err != nil {
		log.Fatalf("ReadPassword err: %v\n", err)
		return nil
	}

	fmt.Printf("Confirm new passphrase: ")
	key2, err2 := terminal.ReadPassword(syscall.Stdin)
	fmt.Println("")

	if err2 != nil {
		log.Fatalf("ReadPassword err: %v\n", err)
		return nil
	}

	if !bytes.Equal(key, key2) {
		log.Fatalf("New passphrases do not match\n")
		return nil
	}

	return key
}

func luksParseExistingKey() []byte {
	var err error
	var ek []byte
	ekf := viper.GetString("key-path")

	if ekf == "" {
		ek = askExistingPass()
	} else {
		ek, err = ioutil.ReadFile(ekf)
		if err != nil {
			log.Fatalf("Failed to read existing key file: %v\n", err)
		}
	}

	if len(ek) < 8 {
		log.Fatalf("Invalid existing key (too short)")
	}

	return ek
}

func luksParseNewKey() []byte {
	var err error
	var nk []byte
	nkf := viper.GetString("new-key-path")

	if nkf == "" {
		nk = askNewPass()
	} else {
		nk, err = ioutil.ReadFile(nkf)
		if err != nil {
			log.Fatalf("Failed to read key file: %v\n", err)
		}
	}

	if len(nk) < 8 {
		log.Fatalf("Invalid new key (too short)")
	}

	return nk
}

func luksReadBypassKey() []byte {
	key, err := ioutil.ReadFile("/sys/class/dmi/id/product_uuid")
	if err != nil {
		log.Fatalf("Failed to read /sys/class/dmi/id/product_uuid: %v\n", err)
	}
	return key
}

var (
	luksAddKeyCmd = &cobra.Command{
		Use:   "add-key",
		Short: "Add LUKS key",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("luks add-key called")
			client := initializeRedctlClient()
			defer client.Close()

			ek := luksParseExistingKey()
			nk := luksParseNewKey()

			_, err := client.LuksAddKey(ek, nk)
			if err != nil {
				log.Fatalf("Failed to add key: %v\n", err)
			}
		},
	}

	luksRemoveKeyCmd = &cobra.Command{
		Use:   "remove-key",
		Short: "Remove LUKS key",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("luks remove-key called")
			client := initializeRedctlClient()
			defer client.Close()

			ek := luksParseExistingKey()

			_, err := client.LuksRemoveKey(ek)
			if err != nil {
				log.Fatalf("Failed to remove keys: %v\n", err)
			}
		},
	}

	luksRotateKeyCmd = &cobra.Command{
		Use:   "rotate-key",
		Short: "Rotate LUKS keys",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("luks rotate-key called")
			client := initializeRedctlClient()
			defer client.Close()

			ek := luksParseExistingKey()
			nk := luksParseNewKey()

			_, err := client.LuksRotateKey(ek, nk)
			if err != nil {
				log.Fatalf("Failed to rotate keys: %v\n", err)
			}
		},
	}

	luksAddBypassKeyCmd = &cobra.Command{
		Use:   "add-bypass-key",
		Short: "Adds key to bypass boot-time authentication",
		Long:  `Use /sys/class/dmi/id/product_uuid as the passphrase key to bypass boot auth prompt`,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("luks add-bypass-key")

			client := initializeRedctlClient()
			defer client.Close()

			ek := luksParseExistingKey()
			nk := luksReadBypassKey()

			_, err := client.LuksAddKey(ek, nk)
			if err != nil {
				log.Fatalf("Failed to add bypass key: %v\n", err)
			}
		},
	}

	luksRemoveBypassKeyCmd = &cobra.Command{
		Use:   "remove-bypass-key",
		Short: "Removes key used to bypass boot-time authentication",
		Long:  `Use /sys/class/dmi/id/product_uuid as the passphrase key to bypass boot auth prompt`,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("luks remove-bypass-key")

			client := initializeRedctlClient()
			defer client.Close()

			ek := luksReadBypassKey()

			_, err := client.LuksRemoveKey(ek)
			if err != nil {
				log.Fatalf("Failed to remove bypass key: %v\n", err)
			}
		},
	}
)

func init() {
	rootCmd.AddCommand(luksCmd)

	luksCmd.AddCommand(luksAddKeyCmd)
	luksAddKeyCmd.Flags().String("key-path", "", "Path to existing key file")
	luksAddKeyCmd.Flags().String("new-key-path", "", "Path to new key file")

	luksCmd.AddCommand(luksAddBypassKeyCmd)
	luksAddBypassKeyCmd.Flags().String("key-path", "", "Path to existing key file")

	luksCmd.AddCommand(luksRemoveKeyCmd)
	luksRemoveKeyCmd.Flags().String("key-path", "", "Path to existing key file (to remove)")

	luksCmd.AddCommand(luksRemoveBypassKeyCmd)

	luksCmd.AddCommand(luksRotateKeyCmd)
	luksRotateKeyCmd.Flags().String("key-path", "", "Path to existing key file")
	luksRotateKeyCmd.Flags().String("new-key-path", "", "Path to new key file")
}
