// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/redfield/redctl/api"
)

// Returns client and domain, exiting on error
func initDomainOp() (*api.Client, *api.Domain) {
	client := initializeRedctlClient()

	uuid := viper.GetString("uuid")
	name := viper.GetString("name")

	domain, err := client.DomainFindByUUIDThenName(uuid, name)
	if err != nil || domain == nil {
		log.Fatalf("Failed to find domain: %v", err)
	}

	return client, domain
}

// vmCmd represents the vm command
var (
	vmCmd = &cobra.Command{
		Use:   "vm",
		Short: "Manage virtual machines",
		Long:  `VM Creation, Copying, Modification, Deletion`,
	}
)

var (
	vmCreateCmd = &cobra.Command{
		Use:   "create",
		Short: "Create user virtual machine",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("creating vm...")
			client := initializeRedctlClient()
			defer client.Close()

			name := viper.GetString("name")
			if name == "" {
				log.Fatal("Domain name is required for creation")
			}
			d := api.NewUserDomain(name)

			if icon := viper.GetString("icon"); icon != "" {
				_, err := client.GraphicFind(icon, api.GraphicType_GRAPHIC_DESKTOP_ICON)
				if err != nil {
					log.Fatalf("Failed to create domain: %v", err)
				}
				d.DesktopIcon = icon
			}

			if viper.GetBool("autostart") {
				d.StartOnBoot = true
			}

			domain, err := client.DomainCreate(d)
			if err != nil {
				log.Fatalf("Failed to create domain: %v", err)
			}

			fmt.Println(domain)
		},
	}
)

var (
	vmListCmd = &cobra.Command{
		Use:   "list",
		Short: "List virtual machines",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client := initializeRedctlClient()

			domains, err := client.DomainFindAll()
			if err != nil {
				log.Fatalf("Failed to find all domains: %v", err)
			}

			fmt.Printf("Name\t\tType\t\tUUID\n")
			for _, domain := range domains {
				name := domain.GetConfig().GetName()
				uuid := domain.GetUuid()
				vmtype := domain.GetConfig().GetType()
				fmt.Printf("%v\t\t%v\t\t%v\n", name, vmtype, uuid)
			}
		},
	}
)

var (
	vmGetCmd = &cobra.Command{
		Use:   "get-property",
		Short: "Get key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			props, err := domain.GetProperty(viper.GetString("key"))
			if err != nil {
				log.Fatalf("Failed to get domain property: %v", err)
			}

			for _, p := range props {
				fmt.Println(p)
			}
		},
	}

	vmSetCmd = &cobra.Command{
		Use:   "set-property",
		Short: "Set key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			err := domain.SetProperty(viper.GetString("key"), viper.GetString("value"))
			if err != nil {
				log.Fatalf("Failed to set domain property: %v", err)
			}

			if err = client.DomainUpdate(domain); err != nil {
				log.Fatalf("Failed to update domain: %v", err)
			}
		},
	}

	vmEditCmd = &cobra.Command{
		Use:   "edit",
		Short: "Edit VM database object",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			data, _ := json.MarshalIndent(domain, "", "    ")

			tmpfile, err := ioutil.TempFile("/tmp", "vm-config-*")
			if err != nil {
				log.Fatal(err)
			}
			defer os.Remove(tmpfile.Name())

			if _, err := tmpfile.Write(data); err != nil {
				log.Fatal(err)
			}

			if err := tmpfile.Close(); err != nil {
				log.Fatal(err)
			}

			editor := os.Getenv("EDITOR")
			if editor == "" {
				editor = "vi"
			}

			editorProc := exec.Command(editor, tmpfile.Name())
			editorProc.Stdin = os.Stdin
			editorProc.Stdout = os.Stdout
			editorProc.Stderr = os.Stderr

			err = editorProc.Start()
			if err != nil {
				log.Fatal(err)
			}
			err = editorProc.Wait()
			if err != nil {
				log.Fatal(err)
			}

			content, err := ioutil.ReadFile(tmpfile.Name())
			if err != nil {
				log.Fatal(err)
			}

			err = json.Unmarshal(content, &domain)
			if err != nil {
				log.Fatal(err)
			}

			if err := client.DomainUpdate(domain); err != nil {
				log.Fatalf("Failed to update domain: %v", err)
			}
		},
	}
)

var (
	vmRemoveCmd = &cobra.Command{
		Use:   "remove",
		Short: "Remove user virtual machine",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm remove called")

			client, domain := initDomainOp()
			defer client.Close()

			err := client.DomainRemove(domain)
			if err != nil {
				log.Fatalf("Failed to remove domain: %v", err)
			}
		},
	}
)

var (
	vmNetworkCmd = &cobra.Command{
		Use:   "network",
		Short: "Manage virtual machine virtual interfaces (subcommand)",
		Long:  `VM Virtual Network Interface Creation, Copying, Modification, Deletion`,
	}

	vmNetworkListCmd = &cobra.Command{
		Use:   "list",
		Short: "List virtual network interfaces",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			networks, err := domain.GetProperty("vif")
			if err != nil {
				log.Fatalf("Failed to list networks: %v", err)
			}

			fmt.Printf("Index\tNetwork\n")
			for i, network := range networks {
				fmt.Printf("%v\t\t%v\n", i, network)
			}
		},
	}

	vmNetworkCreateCmd = &cobra.Command{
		Use:   "create",
		Short: "Create virtual network interface",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			bridge := viper.GetString("bridge")
			backend := viper.GetString("backend")
			mac := viper.GetString("mac")

			if bridge == "" || backend == "" {
				log.Fatal("Backend and bridge required")
			}

			vif := api.NewDomainNetwork(backend, bridge, mac)
			domain.AddNetwork(vif)

			// Update the domain
			if err := client.DomainUpdate(domain); err != nil {
				log.Fatalf("Failed to update domain: %v", err)
			}

			log.Print("Network device added to domain")
		},
	}

	vmNetworkAttachCmd = &cobra.Command{
		Use:   "attach",
		Short: "Attach virtual network interface",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			bridge := viper.GetString("bridge")
			backend := viper.GetString("backend")
			mac := viper.GetString("mac")

			if backend == "" {
				log.Fatal("Backend required to attach network device")
			}

			if bridge == "" {
				log.Fatal("Bridge required to attach network device")
			}

			vif := api.NewDomainNetwork(backend, bridge, mac)

			err := client.DomainHotplugNetworkAttach(domain, vif)
			if err != nil {
				log.Fatalf("Failed to attach network device: %v", err)
			}
		},
	}

	vmNetworkDetachCmd = &cobra.Command{
		Use:   "detach",
		Short: "Detach virtual network interface",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			mac := viper.GetString("mac")
			if mac == "" {
				log.Fatalf("MAC required to detach network device")
			}
			vif := api.NewDomainNetwork("", "", mac)

			err := client.DomainHotplugNetworkDetach(domain, vif)
			if err != nil {
				log.Fatalf("Failed to attach network device: %v", err)
			}
		},
	}

	vmNetworkGetCmd = &cobra.Command{
		Use:   "get-property",
		Short: "Get key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			index := viper.GetInt("index")
			key := viper.GetString("key")

			prop, err := domain.GetArrayProperty("vif", index, key)
			if err != nil {
				log.Fatalf("Failed to get network property: %v", err)
			}
			fmt.Println(prop)
		},
	}

	vmNetworkSetCmd = &cobra.Command{
		Use:   "set-property",
		Short: "Set key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			index := viper.GetInt("index")
			key := viper.GetString("key")
			value := viper.GetString("value")

			err := domain.SetArrayProperty("vif", index, key, value)
			if err != nil {
				log.Fatalf("Failed to set network property: %v", err)
			}

			if err := client.DomainUpdate(domain); err != nil {
				log.Fatalf("Failed to update domain: %v", err)
			}
		},
	}

	vmNetworkRemoveCmd = &cobra.Command{
		Use:   "remove",
		Short: "Remove virtual network interface",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			index := viper.GetInt("index")

			log.Println("vm network remove called")
			err := domain.RemoveNetwork(index)
			if err != nil {
				log.Fatalf("Failed to remove network: %v", err)
			}

			if err := client.DomainUpdate(domain); err != nil {
				log.Fatalf("Failed to update domain: %v", err)
			}
		},
	}
)

var (
	vmUsbctrlCmd = &cobra.Command{
		Use:   "usbctrl",
		Short: "Manage virtual machine USB controllers (subcommand)",
		Long:  `VM Virtual USB Controller Creation, Copying, Modification, Deletion`,
	}

	vmUsbctrlListCmd = &cobra.Command{
		Use:   "list",
		Short: "List USB controllers",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			usbctrls, err := domain.GetProperty("usbctrl")
			if err != nil {
				log.Fatalf("Failed to list usbctrls: %v", err)
			}

			fmt.Printf("Index\tUsbctrl\n")
			for i, usbctrl := range usbctrls {
				fmt.Printf("%v\t\t%v", i, usbctrl)
			}
		},
	}

	vmUsbctrlCreateCmd = &cobra.Command{
		Use:   "create",
		Short: "Create USB controller",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			ctype := viper.GetString("type")
			version := viper.GetString("version")
			usbctrl := api.NewDomainUSBController(ctype, version)

			domain.AddUSBController(usbctrl)

			// Update the domain
			if err := client.DomainUpdate(domain); err != nil {
				log.Fatalf("Failed to update domain: %v", err)
			}
		},
	}

	vmUsbctrlGetCmd = &cobra.Command{
		Use:   "get-property",
		Short: "Get key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			index := viper.GetInt("index")
			key := viper.GetString("key")

			prop, err := domain.GetArrayProperty("usbctrl", index, key)
			if err != nil {
				log.Fatalf("Failed to get usbctrl property: %v", err)
			}
			fmt.Println(prop)
		},
	}

	vmUsbctrlSetCmd = &cobra.Command{
		Use:   "set-property",
		Short: "Set key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			index := viper.GetInt("index")
			key := viper.GetString("key")
			value := viper.GetString("value")

			err := domain.SetArrayProperty("usbctrl", index, key, value)
			if err != nil {
				log.Fatalf("Failed to set usbctrl property: %v", err)
			}

			if err := client.DomainUpdate(domain); err != nil {
				log.Fatalf("Failed to update domain: %v", err)
			}
		},
	}

	vmUsbctrlRemoveCmd = &cobra.Command{
		Use:   "remove",
		Short: "Remove virtual usbctrl interface",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			index := viper.GetInt("index")

			err := domain.RemoveUSBController(index)
			if err != nil {
				log.Fatalf("Failed to remove usbctrl: %v", err)
			}

			if err := client.DomainUpdate(domain); err != nil {
				log.Fatalf("Failed to update domain: %v", err)
			}
		},
	}
)

var (
	vmDiskCmd = &cobra.Command{
		Use:   "disk",
		Short: "Manage virtual machine virtual interfaces (subcommand)",
		Long:  `VM Virtual Disk Interface Creation, Copying, Modification, Deletion`,
	}

	vmDiskListCmd = &cobra.Command{
		Use:   "list",
		Short: "List virtual disk interfaces",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			disks, err := domain.GetProperty("disk")
			if err != nil {
				log.Fatalf("Failed to list disks: %v", err)
			}

			fmt.Printf("Index\tDisk\n")
			for i, disk := range disks {
				fmt.Printf("%v\t\t%v", i, disk)
			}
		},
	}

	vmDiskCreateCmd = &cobra.Command{
		Use:   "create",
		Short: "Create virtual disk interface",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			target := viper.GetString("target")
			format := viper.GetString("format")
			if target == "" {
				log.Fatal("Disk target path required")
			}

			disk := api.NewDomainDisk(target, format)

			if viper.GetBool("cdrom") {
				disk.Devtype = "cdrom"
				disk.Format = "raw"
				disk.Vdev = "hdc"
			}

			domain.AddDisk(disk)

			// Update the domain
			if err := client.DomainUpdate(domain); err != nil {
				log.Fatalf("Failed to update domain: %v", err)
			}

			log.Print("Disk added to domain")

		},
	}

	vmDiskGetCmd = &cobra.Command{
		Use:   "get-property",
		Short: "Get key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			index := viper.GetInt("index")
			key := viper.GetString("key")

			prop, err := domain.GetArrayProperty("disk", index, key)
			if err != nil {
				log.Fatalf("Failed to get disk property: %v", err)
			}
			fmt.Println(prop)
		},
	}

	vmDiskSetCmd = &cobra.Command{
		Use:   "set-property",
		Short: "Set key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			index := viper.GetInt("index")
			key := viper.GetString("key")
			value := viper.GetString("value")

			err := domain.SetArrayProperty("disk", index, key, value)
			if err != nil {
				log.Fatalf("Failed to set disk property: %v", err)
			}

			if err := client.DomainUpdate(domain); err != nil {
				log.Fatalf("Failed to update domain: %v", err)
			}
		},
	}

	vmDiskRemoveCmd = &cobra.Command{
		Use:   "remove",
		Short: "Remove virtual disk interface",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			index := viper.GetInt("index")

			err := domain.RemoveDisk(index)
			if err != nil {
				log.Fatalf("Failed to remove disk: %v", err)
			}

			if err := client.DomainUpdate(domain); err != nil {
				log.Fatalf("Failed to update domain: %v", err)
			}
		},
	}
)

var (
	vmPciCmd = &cobra.Command{
		Use:   "pci",
		Short: "Manage virtual machine PCI pass-through devices (subcommand)",
		Long:  `VM Virtual Pci Interface Assignment`,
	}

	vmPciListCmd = &cobra.Command{
		Use:   "list",
		Short: "List pci pass-through devices",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			pcis, err := domain.GetProperty("pci")
			if err != nil {
				log.Fatalf("Failed to list pci devices: %v", err)
			}

			fmt.Printf("Index\tPCI Device\n")
			for i, pci := range pcis {
				fmt.Printf("%v\t\t%v\n", i, pci)
			}
		},
	}

	vmPciAssignCmd = &cobra.Command{
		Use:   "assign",
		Short: "Assign pci pass-through device",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			address := viper.GetString("address")
			if address == "" {
				log.Fatal("PCI device address required")
			}

			pci := api.NewDomainPCIDevice(address)
			domain.AddPCIDevice(pci)

			// Update the domain
			if err := client.DomainUpdate(domain); err != nil {
				log.Fatalf("Failed to update domain: %v", err)
			}

			log.Print("PCI device added to domain")
		},
	}

	vmPciGetCmd = &cobra.Command{
		Use:   "get-property",
		Short: "Get key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			index := viper.GetInt("index")
			key := viper.GetString("key")

			prop, err := domain.GetArrayProperty("pci", index, key)
			if err != nil {
				log.Fatalf("Failed to get PCI device property: %v", err)
			}
			fmt.Println(prop)
		},
	}

	vmPciSetCmd = &cobra.Command{
		Use:   "set-property",
		Short: "Set key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			index := viper.GetInt("index")
			key := viper.GetString("key")
			value := viper.GetString("value")

			err := domain.SetArrayProperty("pci", index, key, value)
			if err != nil {
				log.Fatalf("Failed to set PCI device property: %v", err)
			}

			if err := client.DomainUpdate(domain); err != nil {
				log.Fatalf("Failed to update domain: %v", err)
			}
		},
	}

	vmPciRemoveCmd = &cobra.Command{
		Use:   "remove",
		Short: "Remove PCI pass-through device",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			address := viper.GetString("address")

			if viper.GetString("index") != "" {
				if err := domain.RemovePCIDeviceByIndex(viper.GetInt("index")); err != nil {
					log.Fatalf("Failed to remove pci device from domain config: %v", err)
				}
			} else if address != "" {
				if err := domain.RemovePCIDeviceByAddress(address); err != nil {
					log.Fatalf("Failed to remove pci device from domain config: %v", err)
				}
			} else {
				log.Fatal("PCI device address required")
			}

			// Update the domain
			if err := client.DomainUpdate(domain); err != nil {
				log.Fatalf("Failed to update domain: %v", err)
			}

			log.Println("PCI device removed from domain")
		},
	}
)

var (
	vmStartCmd = &cobra.Command{
		Use:   "start",
		Short: "Start virtual machine",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm start called")
			client, domain := initDomainOp()
			defer client.Close()

			if err := client.DomainStart(domain.GetUuid()); err != nil {
				log.Fatalf("Failed to start domain: %v", err)
			}
		},
	}
)

var (
	vmStopCmd = &cobra.Command{
		Use:   "stop",
		Short: "Stop virtual machine",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			if err := client.DomainStop(domain.GetUuid()); err != nil {
				log.Fatalf("Failed to stop domain: %v", err)
			}
		},
	}
)

var (
	vmRestartCmd = &cobra.Command{
		Use:   "restart",
		Short: "Restart virtual machine",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client, domain := initDomainOp()
			defer client.Close()

			if err := client.DomainRestart(domain.GetUuid()); err != nil {
				log.Fatalf("Failed to restart domain: %v", err)
			}
		},
	}
)

func init() {
	rootCmd.AddCommand(vmCmd)

	vmCmd.AddCommand(vmCreateCmd)
	vmCreateCmd.Flags().String("uuid", "", "vm uuid")
	vmCreateCmd.Flags().String("name", "", "vm name")
	vmCreateCmd.Flags().String("icon", "", "Optional desktop icon for vm")
	vmCreateCmd.Flags().Bool("autostart", false, "Autostart domain at boot time")

	vmCmd.AddCommand(vmGetCmd)
	vmGetCmd.Flags().String("uuid", "", "vm uuid")
	vmGetCmd.Flags().String("name", "", "vm name")
	vmGetCmd.Flags().String("key", "", "property key")

	vmCmd.AddCommand(vmEditCmd)
	vmEditCmd.Flags().String("uuid", "", "vm uuid")
	vmEditCmd.Flags().String("name", "", "vm name")

	vmCmd.AddCommand(vmListCmd)

	vmCmd.AddCommand(vmRemoveCmd)
	vmRemoveCmd.Flags().String("uuid", "", "vm uuid")
	vmRemoveCmd.Flags().String("name", "", "vm name")

	vmCmd.AddCommand(vmRestartCmd)
	vmRestartCmd.Flags().String("uuid", "", "vm uuid")
	vmRestartCmd.Flags().String("name", "", "vm name")

	vmCmd.AddCommand(vmSetCmd)
	vmSetCmd.Flags().String("uuid", "", "vm uuid")
	vmSetCmd.Flags().String("name", "", "vm name")
	vmSetCmd.Flags().String("key", "", "property key")
	vmSetCmd.Flags().String("value", "", "property value")

	vmCmd.AddCommand(vmStartCmd)
	vmStartCmd.Flags().String("uuid", "", "vm uuid")
	vmStartCmd.Flags().String("name", "", "vm name")

	vmCmd.AddCommand(vmStopCmd)
	vmStopCmd.Flags().String("uuid", "", "vm uuid")
	vmStopCmd.Flags().String("name", "", "vm name")

	vmCmd.AddCommand(vmNetworkCmd)

	vmNetworkCmd.AddCommand(vmNetworkCreateCmd)
	vmNetworkCreateCmd.Flags().String("uuid", "", "vm uuid")
	vmNetworkCreateCmd.Flags().String("name", "", "vm name")
	vmNetworkCreateCmd.Flags().String("backend", "", "backend domain for network device")
	vmNetworkCreateCmd.Flags().String("bridge", "", "bridge for network device")
	vmNetworkCreateCmd.Flags().String("mac", "", "MAC address of network device")

	vmNetworkCmd.AddCommand(vmNetworkGetCmd)
	vmNetworkGetCmd.Flags().String("uuid", "", "vm uuid")
	vmNetworkGetCmd.Flags().String("name", "", "vm name")
	vmNetworkGetCmd.Flags().String("key", "", "property key")
	vmNetworkGetCmd.Flags().Int("index", 0, "vm virtual network index")

	vmNetworkCmd.AddCommand(vmNetworkListCmd)
	vmNetworkListCmd.Flags().String("uuid", "", "vm uuid")
	vmNetworkListCmd.Flags().String("name", "", "vm name")

	vmNetworkCmd.AddCommand(vmNetworkSetCmd)
	vmNetworkSetCmd.Flags().String("uuid", "", "vm uuid")
	vmNetworkSetCmd.Flags().String("name", "", "vm name")
	vmNetworkSetCmd.Flags().String("key", "", "property key")
	vmNetworkSetCmd.Flags().String("value", "", "property value")
	vmNetworkSetCmd.Flags().Int("index", 0, "vm virtual network index")

	vmNetworkCmd.AddCommand(vmNetworkAttachCmd)
	vmNetworkAttachCmd.Flags().String("uuid", "", "vm uuid")
	vmNetworkAttachCmd.Flags().String("name", "", "vm name")
	vmNetworkAttachCmd.Flags().String("backend", "", "backend domain for network device")
	vmNetworkAttachCmd.Flags().String("bridge", "", "bridge for network device")
	vmNetworkAttachCmd.Flags().String("mac", "", "MAC address of network device")

	vmNetworkCmd.AddCommand(vmNetworkDetachCmd)
	vmNetworkDetachCmd.Flags().String("uuid", "", "vm uuid")
	vmNetworkDetachCmd.Flags().String("name", "", "vm name")
	vmNetworkDetachCmd.Flags().String("mac", "", "MAC address of network device")

	vmNetworkCmd.AddCommand(vmNetworkRemoveCmd)
	vmNetworkRemoveCmd.Flags().String("uuid", "", "vm uuid")
	vmNetworkRemoveCmd.Flags().String("name", "", "vm name")
	vmNetworkRemoveCmd.Flags().Int("index", 0, "vm virtual network index")

	vmCmd.AddCommand(vmDiskCmd)

	vmDiskCmd.AddCommand(vmDiskCreateCmd)
	vmDiskCreateCmd.Flags().String("uuid", "", "vm uuid")
	vmDiskCreateCmd.Flags().String("name", "", "vm name")
	vmDiskCreateCmd.Flags().String("target", "", "path to target disk")
	vmDiskCreateCmd.Flags().String("format", "qcow2", "disk format")
	vmDiskCreateCmd.Flags().Bool("cdrom", false, "device type is cdrom, e.g. for installation media")

	vmDiskCmd.AddCommand(vmDiskGetCmd)
	vmDiskGetCmd.Flags().String("uuid", "", "vm uuid")
	vmDiskGetCmd.Flags().String("name", "", "vm name")
	vmDiskGetCmd.Flags().Int("index", 0, "vm virtual disk index")
	vmDiskGetCmd.Flags().String("key", "", "property key")

	vmDiskCmd.AddCommand(vmDiskListCmd)
	vmDiskListCmd.Flags().String("uuid", "", "vm uuid")
	vmDiskListCmd.Flags().String("name", "", "vm name")

	vmDiskCmd.AddCommand(vmDiskSetCmd)
	vmDiskSetCmd.Flags().String("uuid", "", "vm uuid")
	vmDiskSetCmd.Flags().String("name", "", "vm name")
	vmDiskSetCmd.Flags().Int("index", 0, "vm virtual disk index")
	vmDiskSetCmd.Flags().String("key", "", "property key")
	vmDiskSetCmd.Flags().String("value", "", "property value")

	vmDiskCmd.AddCommand(vmDiskRemoveCmd)
	vmDiskRemoveCmd.Flags().String("uuid", "", "vm uuid")
	vmDiskRemoveCmd.Flags().String("name", "", "vm name")
	vmDiskRemoveCmd.Flags().Int("index", 0, "vm virtual disk index")

	vmCmd.AddCommand(vmUsbctrlCmd)

	vmUsbctrlCmd.AddCommand(vmUsbctrlCreateCmd)
	vmUsbctrlCreateCmd.Flags().String("uuid", "", "vm uuid")
	vmUsbctrlCreateCmd.Flags().String("name", "", "vm name")
	vmUsbctrlCreateCmd.Flags().String("type", "devicemodel", "USB controller type")
	vmUsbctrlCreateCmd.Flags().String("version", "1", "USB version for the controller")

	vmUsbctrlCmd.AddCommand(vmUsbctrlGetCmd)
	vmUsbctrlGetCmd.Flags().String("uuid", "", "vm uuid")
	vmUsbctrlGetCmd.Flags().String("name", "", "vm name")
	vmUsbctrlGetCmd.Flags().Int("index", 0, "usb controller index")
	vmUsbctrlGetCmd.Flags().String("key", "", "property key")

	vmUsbctrlCmd.AddCommand(vmUsbctrlListCmd)
	vmUsbctrlListCmd.Flags().String("uuid", "", "vm uuid")
	vmUsbctrlListCmd.Flags().String("name", "", "vm name")

	vmUsbctrlCmd.AddCommand(vmUsbctrlSetCmd)
	vmUsbctrlSetCmd.Flags().String("uuid", "", "vm uuid")
	vmUsbctrlSetCmd.Flags().String("name", "", "vm name")
	vmUsbctrlSetCmd.Flags().Int("index", 0, "usb controller index")
	vmUsbctrlSetCmd.Flags().String("key", "", "property key")
	vmUsbctrlSetCmd.Flags().String("value", "", "property value")

	vmUsbctrlCmd.AddCommand(vmUsbctrlRemoveCmd)
	vmUsbctrlRemoveCmd.Flags().String("uuid", "", "vm uuid")
	vmUsbctrlRemoveCmd.Flags().String("name", "", "vm name")
	vmUsbctrlRemoveCmd.Flags().Int("index", 0, "usb controller index")

	vmCmd.AddCommand(vmPciCmd)

	vmPciCmd.AddCommand(vmPciAssignCmd)
	vmPciAssignCmd.Flags().String("uuid", "", "vm uuid")
	vmPciAssignCmd.Flags().String("name", "", "vm name")
	vmPciAssignCmd.Flags().String("address", "", "pci device address")

	vmPciCmd.AddCommand(vmPciGetCmd)
	vmPciGetCmd.Flags().String("uuid", "", "vm uuid")
	vmPciGetCmd.Flags().String("name", "", "vm name")
	vmPciGetCmd.Flags().Int("index", 0, "vm virtual pci index")
	vmPciGetCmd.Flags().String("key", "", "property key")

	vmPciCmd.AddCommand(vmPciListCmd)
	vmPciListCmd.Flags().String("uuid", "", "vm uuid")
	vmPciListCmd.Flags().String("name", "", "vm name")

	vmPciCmd.AddCommand(vmPciRemoveCmd)
	vmPciRemoveCmd.Flags().String("uuid", "", "vm uuid")
	vmPciRemoveCmd.Flags().String("name", "", "vm name")
	vmPciRemoveCmd.Flags().String("address", "", "pci device address")
	vmPciRemoveCmd.Flags().Int("index", 0, "vm virtual pci index")

	vmPciCmd.AddCommand(vmPciSetCmd)
	vmPciSetCmd.Flags().String("uuid", "", "vm uuid")
	vmPciSetCmd.Flags().String("name", "", "vm name")
	vmPciSetCmd.Flags().Int("index", 0, "vm virtual pci index")
	vmPciSetCmd.Flags().String("key", "", "property key")
	vmPciSetCmd.Flags().String("value", "", "property value")
}
