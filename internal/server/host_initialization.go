// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"gitlab.com/redfield/redctl/api"
)

const (
	hostUUID = "00000000-0000-0000-0000-000000000000"
)

func (s *server) initializeHost() error {
	go s.domainEventHandler()

	if err := s.createDom0Entry(); err != nil {
		log.Printf("Failed to create db entry for dom0: %v", err)
		return err
	}

	// Start domains that don't require UI as soon as system is ready.
	// This minimizes the time required to boot a functional NDVM and
	// other backend VMs that may be required for operation.
	go func() {
		err := s.startPVDomains()
		if err != nil {
			log.Printf("Fatal error attempting to start PV domains: %v\n", err)
		}
	}()

	return nil
}

func (s *server) domainEventHandler() {
	el := s.eventPublisher.registerSubscriber()
	defer s.eventPublisher.unregisterSubscriber(el)

	for {
		event := <-el.update

		switch event.GetType() {

		case api.Event_DOMAIN_STARTED:
			err := s.handleEventDomainStarted(event.GetDomain())
			if err != nil {
				log.Print(err)
			}

		case api.Event_NETWORK_ATTACH:
			err := s.handleEventNetworkAttach(event.GetDomain(), event.GetNetwork())
			if err != nil {
				log.Print(err)
			}

		case api.Event_NETWORK_DETACH:
			err := s.handleEventNetworkDetach(event.GetDomain(), event.GetNetwork())
			if err != nil {
				log.Print(err)
			}

		default:
			log.Printf("Event not handled by redctld: %v", event.GetType())
		}
	}
}

func (s *server) handleEventDomainStarted(domain *api.Domain) error {
	host, err := s.domainRepo.Find(hostUUID)
	if err != nil {
		return err
	}

	for _, n := range host.GetConfig().GetNetworks() {
		if n.GetBackend() != domain.GetConfig().GetName() {
			continue
		}

		go s.attachNetworkWithRetry(host, n)

		// Successfully handled event
		return nil
	}

	// Domain was not a backend
	return nil
}

func (s *server) handleEventNetworkAttach(domain *api.Domain, network *api.DomainNetwork) error {
	// If the domain is dom0, create a systemd network entry
	if domain.GetUuid() == hostUUID {
		return createSystemdNetworkEntry(network)
	}

	return nil
}

func (s *server) handleEventNetworkDetach(domain *api.Domain, network *api.DomainNetwork) error {
	// If the domain is dom0, remove the systemd network entry
	if domain.GetUuid() == hostUUID {
		return removeSystemdNetworkEntry(network)
	}

	return nil
}

func (s *server) shutdown(timeout uint32) {
	// Detach dom0 network interfaces.
	err := s.detachNetworksFromHost()
	if err != nil {
		log.Printf("Failed to detach host network interfaces: %v", err)
	}

	err = s.forceStopAllDomains(timeout)
	if err != nil {
		log.Printf("Failed to force stop all domains: %v", err)
	}
}

func (s *server) forceStopAllDomains(timeout uint32) error {
	rd, err := s.xenDriver.RunningDomainsByName()
	if err != nil {
		return fmt.Errorf("could not get running domains: %v", err)
	}

	var wg sync.WaitGroup
	wg.Add(len(rd) + 1)

	go func() {
		defer wg.Done()

		for {
			select {
			case <-time.After(time.Duration(timeout) * time.Second):
				return
			default:
				time.Sleep(1 * time.Second)

				rd, err := s.xenDriver.RunningDomainsByName()
				if err != nil {
					log.Printf("Failed to get running domains: %v", err)
				}

				// dom0 is the only running domain.
				if len(rd) == 1 {
					return
				}
			}
		}
	}()

	for _, name := range rd {
		d := &api.Domain{
			Config: &api.DomainConfiguration{Name: name},
		}

		go func(domain *api.Domain) {
			defer wg.Done()

			if err := s.forceStopDomain(domain, timeout); err != nil {
				log.Printf("Failed to stop domain: %v", err)
			}
		}(d)
	}

	wg.Wait()

	return nil
}

func (s *server) forceStopDomain(domain *api.Domain, timeout uint32) error {
	if domain.GetConfig().GetName() == "Domain-0" {
		return nil
	}

	done := make(chan bool)
	go func() {
		for {
			select {
			case <-done:
				return

			case <-time.After(time.Duration(timeout) * time.Second):
				log.Printf("Domain stop exceeded timeout of %v seconds", timeout)
				log.Printf("Destroying domain %v", domain.GetConfig().GetName())

				_ = s.xenDriver.DestroyDomain(*domain)
			}
		}
	}()

	log.Printf("Stopping domain %v", domain.GetConfig().GetName())
	err := s.xenDriver.StopDomain(*domain)

	done <- true

	return err
}

func (s *server) attachNetworkWithRetry(domain *api.Domain, network *api.DomainNetwork) {
	// Try up to 10 times to attach the network
	name := domain.GetConfig().GetName()

	for i := 0; i < 10; i++ {
		time.Sleep(1 * time.Second)

		if err := s.domainNetworkAttach(domain, network); err == nil {
			log.Printf("Network attached to %v: %v", name, network)
			return
		}

		log.Printf("Failed to attach network to %v, retrying in one second...", name)
	}

	log.Printf("Failed to attach network to %v after 10 tries", name)
}

func (s *server) detachNetworksFromHost() error {
	domain, err := s.domainRepo.Find(hostUUID)
	if err != nil {
		return err
	}

	out, err := exec.Command("xenstore-list", "/local/domain/0/device/vif").CombinedOutput()
	if err != nil {
		return fmt.Errorf("%v: %v", string(out), err)
	}

	devids := strings.Fields(string(out))
	for _, devid := range devids {
		n := &api.DomainNetwork{Devid: devid}
		if err = s.domainNetworkDetach(domain, n); err != nil {
			log.Printf("Failed to detach network device %v from host", devid)
		}
	}

	return nil
}

func (s *server) startPVDomains() error {
	domains, err := s.domainRepo.FindAll()
	if err != nil {
		return err
	}

	for _, dom := range domains {
		if !dom.GetStartOnBoot() || dom.GetConfig().GetType() != "pv" {
			continue
		}

		if err := s.domainStartWithDependencies(dom); err != nil {
			// ignore errors starting domains, but log them
			log.Printf("failed to start pv domain %v: %v\n", dom.Config.Name, err)
		}
	}

	return nil
}

func (s *server) createDom0Entry() error {
	// If there is no entry for dom0, create it
	if _, err := s.domainRepo.Find(hostUUID); err != nil {
		d := &api.Domain{
			Uuid: hostUUID,
			Config: &api.DomainConfiguration{
				Name: "Domain-0",
			},
		}
		err := s.domainRepo.Create(d)
		if err != nil {
			return err
		}
	}

	return nil
}

var (
	systemdNetworkEntryName = "20-redfield-%v.network"

	systemdNetworkEntry = `[Match]
MACAddress=%v

[Network]
DHCP=ipv4

[DHCP]
UseDomains=true
`
)

const systemdNetworkDir = "/run/systemd/network"

func createSystemdNetworkEntry(network *api.DomainNetwork) error {
	mac := strings.ReplaceAll(network.GetMac(), ":", "")
	name := fmt.Sprintf(systemdNetworkEntryName, mac)

	entry := fmt.Sprintf(systemdNetworkEntry, network.GetMac())
	path := filepath.Join(systemdNetworkDir, name)

	err := ioutil.WriteFile(path, []byte(entry), 0644)
	if err != nil {
		return fmt.Errorf("unable to create network entry: %v", err)
	}

	err = exec.Command("systemctl", "restart", "systemd-networkd").Run()
	if err != nil {
		return fmt.Errorf("unable to restart systemd-networkd: %v", err)
	}

	return nil
}

func removeSystemdNetworkEntry(network *api.DomainNetwork) error {
	mac := strings.ReplaceAll(network.GetMac(), ":", "")
	name := fmt.Sprintf(systemdNetworkEntryName, mac)
	path := filepath.Join(systemdNetworkDir, name)

	err := os.Remove(path)
	if err != nil {
		return fmt.Errorf("unable to remove network entry: %v", err)
	}

	return nil
}
