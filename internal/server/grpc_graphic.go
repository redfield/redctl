// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/redfield/redctl/api"
)

func (s *server) GraphicImport(ctx context.Context, in *api.GraphicImportRequest) (*api.GraphicImportReply, error) {
	g := in.GetGraphic()
	if g == nil {
		return nil, errors.New("received nil graphic")
	}

	if err := s.graphicsRepo.Import(g.GetName(), g.GetType(), g.GetData()); err != nil {
		return nil, err
	}

	return &api.GraphicImportReply{}, nil
}

func (s *server) GraphicFind(ctx context.Context, in *api.GraphicFindRequest) (*api.GraphicFindReply, error) {
	g := in.GetGraphic()

	path := s.graphicsRepo.Find(g.GetName(), g.GetType())
	if path == "" {
		return nil, fmt.Errorf("graphic '%v' does not exist", g.GetName())
	}
	g.Path = path

	return &api.GraphicFindReply{Graphic: g}, nil
}

func (s *server) GraphicFindAll(ctx context.Context, in *api.GraphicFindAllRequest) (*api.GraphicFindAllReply, error) {
	graphics, err := s.graphicsRepo.FindAll(in.GetType())
	if err != nil {
		return nil, fmt.Errorf("failed to find graphics: %v", err)
	}

	return &api.GraphicFindAllReply{Graphics: graphics}, nil
}

func (s *server) GraphicRemove(ctx context.Context, in *api.GraphicRemoveRequest) (*api.GraphicRemoveReply, error) {
	g := in.GetGraphic()

	err := s.graphicsRepo.Remove(g.GetName(), g.GetType())
	if err != nil {
		return nil, err
	}

	return &api.GraphicRemoveReply{}, nil
}
