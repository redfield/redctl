// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package repository

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"sort"
	"testing"

	"gitlab.com/redfield/redctl/api"
)

func TestGraphicImport(t *testing.T) {
	graphicsDir, err := ioutil.TempDir("", "graphics")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(graphicsDir)

	repo := NewGraphicsRepo(graphicsDir)

	data := make([]byte, 256)

	gPNG := &api.Graphic{
		Name: "graphic.png",
		Data: data,
		Type: api.GraphicType_GRAPHIC_BACKGROUND,
	}
	gSVG := &api.Graphic{
		Name: "graphic.svg",
		Data: data,
		Type: api.GraphicType_GRAPHIC_DESKTOP_ICON,
	}
	gTXT := &api.Graphic{
		Name: "graphic.txt",
		Data: data,
	}

	if err := repo.Import(gPNG.GetName(), gPNG.GetType(), gPNG.GetData()); err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if err := repo.Import(gSVG.GetName(), gSVG.GetType(), gSVG.GetData()); err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if err := repo.Import(gTXT.GetName(), gTXT.GetType(), gTXT.GetData()); err == nil {
		t.Error("Expected error when using unsupported format")
	}

}

func TestGraphicFind(t *testing.T) {
	graphicsDir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(graphicsDir)

	repo := NewGraphicsRepo(graphicsDir)

	f1, err := ioutil.TempFile(graphicsDir, "*.png")
	if err != nil {
		t.Errorf("Failed to create tmp file: %v", err)
	}
	defer os.Remove(f1.Name())
	f1BaseName := filepath.Base(f1.Name())

	f2, err := ioutil.TempFile("", "*.png")
	if err != nil {
		t.Errorf("Failed to create tmp file: %v", err)
	}
	defer os.Remove(f2.Name())
	f2BaseName := filepath.Base(f2.Name())

	if path := repo.Find(f1BaseName, api.GraphicType_GRAPHIC_UNSPECIFIED); path == "" {
		t.Error("Expected to receive non-empty path")
	}

	if path := repo.Find(f2BaseName, api.GraphicType_GRAPHIC_UNSPECIFIED); path != "" {
		t.Errorf("Expected empty result but received '%v'", path)
	}
}

func TestGraphicRemove(t *testing.T) {
	graphicsDir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(graphicsDir)

	repo := NewGraphicsRepo(graphicsDir)

	f1, err := ioutil.TempFile(graphicsDir, "*.png")
	if err != nil {
		t.Errorf("Failed to create tmp file: %v", err)
	}
	defer os.Remove(f1.Name())
	f1BaseName := filepath.Base(f1.Name())

	f2, err := ioutil.TempFile("", "*.png")
	if err != nil {
		t.Errorf("Failed to create tmp file: %v", err)
	}
	defer os.Remove(f2.Name())
	f2BaseName := filepath.Base(f2.Name())

	if err := repo.Remove(f1BaseName, api.GraphicType_GRAPHIC_UNSPECIFIED); err != nil {
		t.Errorf("Failed to remove graphic from repo: %v", err)
	}

	if err := repo.Remove(f2BaseName, api.GraphicType_GRAPHIC_UNSPECIFIED); err == nil {
		t.Errorf("Expected error removing non-existent graphic from repo: %v", f2.Name())
	}
}

func TestValidExtension(t *testing.T) {
	table := map[string]bool{
		".jpg":     false,
		".jpeg":    false,
		FileExtPNG: true,
		FileExtSVG: true,
	}

	for ext, valid := range table {
		if isValidGraphicsExtension(fmt.Sprintf("test%v", ext)) != valid {
			t.Fatalf("expected isValidGraphicsExtension to return %v for ext=%v", valid, ext)
		}
	}
}

func TestGraphicFindAll(t *testing.T) {
	graphicsDir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(graphicsDir)

	repo := NewGraphicsRepo(graphicsDir)

	files := []string{
		repo.absPath("test1.txt", api.GraphicType_GRAPHIC_UNSPECIFIED),
		repo.absPath("test2.png", api.GraphicType_GRAPHIC_UNSPECIFIED),
		repo.absPath("test3.svg", api.GraphicType_GRAPHIC_UNSPECIFIED),
		repo.absPath("test4.png", api.GraphicType_GRAPHIC_BACKGROUND),
		repo.absPath("test5.svg", api.GraphicType_GRAPHIC_DESKTOP_ICON),
	}
	validFiles := files[1:5]

	for _, f := range files {
		if err := os.MkdirAll(filepath.Dir(f), 0755); err != nil {
			t.Fatalf("Failed to create directory: %v", err)
		}
		if err := ioutil.WriteFile(f, []byte{}, 0644); err != nil {
			t.Fatalf("Failed to write temp file: %v", err)
		}
	}

	graphics, err := repo.FindAll(api.GraphicType_GRAPHIC_UNSPECIFIED)
	if err != nil {
		t.Fatalf("Unexpected error when calling FindAll: %v", err)
	}

	paths := make([]string, 0)
	for _, g := range graphics {
		paths = append(paths, g.GetPath())
	}

	sg := sort.StringSlice(paths)
	sv := sort.StringSlice(validFiles)
	sg.Sort()
	sv.Sort()

	if !reflect.DeepEqual(sv, sg) {
		t.Fatalf("Expected: %v\n Received: %v", sv, sg)
	}
}

func TestGraphicFindAllDesktopIcons(t *testing.T) {
	graphicsDir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(graphicsDir)

	repo := NewGraphicsRepo(graphicsDir)

	files := []string{
		repo.absPath("test1.txt", api.GraphicType_GRAPHIC_UNSPECIFIED),
		repo.absPath("test2.svg", api.GraphicType_GRAPHIC_UNSPECIFIED),
		repo.absPath("test3.png", api.GraphicType_GRAPHIC_BACKGROUND),
		repo.absPath("test4.svg", api.GraphicType_GRAPHIC_DESKTOP_ICON),
		repo.absPath("test5.png", api.GraphicType_GRAPHIC_DESKTOP_ICON),
	}
	validFiles := files[3:5]

	for _, f := range files {
		if err := os.MkdirAll(filepath.Dir(f), 0755); err != nil {
			t.Fatalf("Failed to create directory: %v", err)
		}
		if err := ioutil.WriteFile(f, []byte{}, 0644); err != nil {
			t.Fatalf("Failed to write temp file: %v", err)
		}
	}

	graphics, err := repo.FindAll(api.GraphicType_GRAPHIC_DESKTOP_ICON)
	if err != nil {
		t.Fatalf("Unexpected error when calling FindAll: %v", err)
	}

	paths := make([]string, 0)
	for _, g := range graphics {
		paths = append(paths, g.GetPath())
	}

	sg := sort.StringSlice(paths)
	sv := sort.StringSlice(validFiles)
	sg.Sort()
	sv.Sort()

	if !reflect.DeepEqual(sv, sg) {
		t.Fatalf("Expected: %v\n Received: %v", sv, sg)
	}
}

func TestGraphicFindAllBackgrounds(t *testing.T) {
	graphicsDir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(graphicsDir)

	repo := NewGraphicsRepo(graphicsDir)

	files := []string{
		repo.absPath("test1.txt", api.GraphicType_GRAPHIC_UNSPECIFIED),
		repo.absPath("test2.svg", api.GraphicType_GRAPHIC_UNSPECIFIED),
		repo.absPath("test3.png", api.GraphicType_GRAPHIC_DESKTOP_ICON),
		repo.absPath("test4.png", api.GraphicType_GRAPHIC_BACKGROUND),
		repo.absPath("test5.svg", api.GraphicType_GRAPHIC_BACKGROUND),
	}
	validFiles := files[3:5]

	for _, f := range files {
		if err := os.MkdirAll(filepath.Dir(f), 0755); err != nil {
			t.Fatalf("Failed to create directory: %v", err)
		}
		if err := ioutil.WriteFile(f, []byte{}, 0644); err != nil {
			t.Fatalf("Failed to write temp file: %v", err)
		}
	}

	graphics, err := repo.FindAll(api.GraphicType_GRAPHIC_BACKGROUND)
	if err != nil {
		t.Fatalf("Unexpected error when calling FindAll: %v", err)
	}

	paths := make([]string, 0)
	for _, g := range graphics {
		paths = append(paths, g.GetPath())
	}

	sg := sort.StringSlice(paths)
	sv := sort.StringSlice(validFiles)
	sg.Sort()
	sv.Sort()

	if !reflect.DeepEqual(sv, sg) {
		t.Fatalf("Expected: %v\n Received: %v", sv, sg)
	}
}
