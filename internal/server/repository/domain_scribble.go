// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package repository

import (
	"encoding/json"
	"errors"
	"fmt"

	scribble "github.com/nanobox-io/golang-scribble"

	"gitlab.com/redfield/redctl/api"
)

const (
	collectionDomains = "domains"
)

type scribbleDomainRepo struct {
	db *scribble.Driver
}

// NewScribbleDomainRepo create new domain repo using scribble database
func NewScribbleDomainRepo(db *scribble.Driver) DomainRepo {
	return &scribbleDomainRepo{db: db}
}

func (s *scribbleDomainRepo) Find(uuid string) (*api.Domain, error) {
	var domain api.Domain

	err := s.db.Read(collectionDomains, uuid, &domain)
	return &domain, err
}

func (s *scribbleDomainRepo) FindAll() ([]*api.Domain, error) {
	domains := make([]*api.Domain, 0)

	records, err := s.db.ReadAll(collectionDomains)
	if err != nil {
		return domains, fmt.Errorf("could not read domains from database: %v", err)
	}

	for _, r := range records {
		var domain api.Domain
		if err := json.Unmarshal([]byte(r), &domain); err != nil {
			return domains, fmt.Errorf("could not read domains from database: %v", err)
		}

		domains = append(domains, &domain)
	}

	return domains, nil
}

func (s *scribbleDomainRepo) Create(domain *api.Domain) error {
	exists, err := s.domainExists(domain)
	if err != nil {
		return fmt.Errorf("could not determine if domain already exists: %v", err)
	}

	if exists {
		return errors.New("domain already exists")
	}

	return s.db.Write(collectionDomains, domain.GetUuid(), domain)
}

func (s *scribbleDomainRepo) Update(domain *api.Domain) error {
	uuid := domain.GetUuid()
	if _, err := s.Find(uuid); err != nil {
		return errors.New("non-existent domain")
	}
	return s.db.Write(collectionDomains, uuid, domain)
}

func (s *scribbleDomainRepo) Remove(uuid string) error {
	if _, err := s.Find(uuid); err != nil {
		return errors.New("non-existent domain")
	}
	return s.db.Delete(collectionDomains, uuid)
}

func (s *scribbleDomainRepo) domainExists(domain *api.Domain) (bool, error) {
	uuid := domain.GetUuid()
	name := domain.GetConfig().GetName()

	domains, err := s.FindAll()
	if err != nil {
		return false, err
	}

	for _, d := range domains {
		if d.GetConfig().GetName() == name || d.GetUuid() == uuid {
			return true, nil
		}
	}

	return false, nil
}
