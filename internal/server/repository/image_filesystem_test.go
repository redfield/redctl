// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package repository

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/redfield/redctl/api"
)

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}

	return !info.IsDir()
}

func TestCreate(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(dir)

	repo := NewFilesystemImageRepo(dir)

	var image *api.Image
	image, err = repo.Create("test1.qcow2", 100000)
	if err != nil {
		t.Errorf("Failed to create test1 qcow image: %v\n", err)
	}

	if image == nil {
		t.Errorf("Failed to create test1 qcow image: image nil\n")
	}

	if !fileExists(filepath.Join(dir, "test1.qcow2")) {
		t.Errorf("Failed to create test1 qcow image: file does not exist\n")
	}

	image, err = repo.Create("test2.raw", 100000)
	if err != nil {
		t.Errorf("Failed to create test2 raw image: %v\n", err)
	}

	if image == nil {
		t.Errorf("Failed to create test2 raw image: image nil\n")
	}

	if !fileExists(filepath.Join(dir, "test2.raw")) {
		t.Errorf("Failed to create test2 raw image: file does not exist")
	}
}

func TestCopy(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(dir)

	repo := NewFilesystemImageRepo(dir)

	var image *api.Image
	image, err = repo.Create("test.qcow2", 100000)
	if err != nil {
		t.Errorf("Failed to create test qcow image: %v\n", err)
	}

	if image == nil {
		t.Errorf("Failed to create test qcow image: image nil\n")
	}

	if !fileExists(filepath.Join(dir, "test.qcow2")) {
		t.Errorf("Failed to create test qcow image: file does not exist\n")
	}

	_, err = repo.Copy("test.qcow2", "test-copy.qcow2")
	if err != nil {
		t.Errorf("Failed to create test copy of test.qcow2 %v\n", err)
	}

	if !fileExists(filepath.Join(dir, "test-copy.qcow2")) {
		t.Errorf("Failed to create test-copy qcow2 image: file does not exist\n")
	}

	image, err = repo.Create("test.raw", 100000)
	if err != nil {
		t.Errorf("Failed to create test raw image: %v\n", err)
	}

	if image == nil {
		t.Errorf("Failed to create test raw image: image nil\n")
	}

	if !fileExists(filepath.Join(dir, "test.raw")) {
		t.Errorf("Failed to create test raw image: file does not exist")
	}

	_, err = repo.Copy("test.raw", "test-copy.raw")
	if err != nil {
		t.Errorf("Failed to create test copy of test.raw %v\n", err)
	}

	if !fileExists(filepath.Join(dir, "test-copy.raw")) {
		t.Errorf("Failed to create test-copy raw image: file does not exist\n")
	}
}

func TestFind(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(dir)

	repo := NewFilesystemImageRepo(dir)

	_, err = repo.Find("")
	if err == nil {
		t.Fatalf("Find should fail on empty name\n")
	}

	var image *api.Image
	image, err = repo.Find("x")
	if err != nil {
		t.Fatalf("Failed on empty image repo: %v\n", err)
	}

	if image != nil {
		t.Fatalf("Image non-nil on empty repo\n")
	}

	_, err = os.OpenFile(filepath.Join(dir, "image1.qcow2"), os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		t.Fatalf("Failed to create image1.qcow2: %v\n", err)
	}
	_, err = os.OpenFile(filepath.Join(dir, "image2.iso"), os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		t.Fatalf("Failed to create image2.iso: %v\n", err)
	}
	_, err = os.OpenFile(filepath.Join(dir, "image3.raw"), os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		t.Fatalf("Failed to create image3.raw: %v\n", err)
	}

	image, err = repo.Find("image2.iso")
	if err != nil {
		t.Fatalf("Failed on populated image repo: %v\n", err)
	}

	if image == nil {
		t.Fatalf("Image nil on populated repo\n")
	}
}

func TestFindAll(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(dir)

	repo := NewFilesystemImageRepo(dir)

	images, err := repo.FindAll()
	if err != nil {
		t.Fatalf("FindAll failed on empty repo\n")
	}

	if len(images) > 0 {
		t.Fatalf("FindAll returned hits on empty repo: %v\n", images)
	}

	_, err = os.OpenFile(filepath.Join(dir, "image1.qcow2"), os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		t.Fatalf("Failed to create image1.qcow2: %v\n", err)
	}
	_, err = os.OpenFile(filepath.Join(dir, "image2.iso"), os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		t.Fatalf("Failed to create image2.iso: %v\n", err)
	}
	_, err = os.OpenFile(filepath.Join(dir, "image3.raw"), os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		t.Fatalf("Failed to create image3.raw: %v\n", err)
	}

	images, err = repo.FindAll()
	if err != nil {
		t.Fatalf("Failed on populated image repo: %v\n", err)
	}

	if images == nil {
		t.Fatalf("Image nil on populated repo\n")
	}

	if len(images) != 3 {
		t.Fatalf("Incorrect number of images for populated repo: %v\n", images)
	}
}

func TestRemove(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(dir)

	repo := NewFilesystemImageRepo(dir)

	err = repo.Remove("")
	if err == nil {
		t.Fatalf("Should fail on removing empty string\n")
	}

	err = repo.Remove("fff")
	if err == nil {
		t.Fatalf("Should fail on removing invalid image name\n")
	}

	_, err = os.OpenFile(filepath.Join(dir, "image1.qcow2"), os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		t.Fatalf("Failed to create image1.qcow2: %v\n", err)
	}
	_, err = os.OpenFile(filepath.Join(dir, "image2.iso"), os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		t.Fatalf("Failed to create image2.iso: %v\n", err)
	}
	_, err = os.OpenFile(filepath.Join(dir, "image3.raw"), os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		t.Fatalf("Failed to create image3.raw: %v\n", err)
	}

	err = repo.Remove("image2.iso")
	if err != nil {
		t.Fatalf("Failed to remove image from populated image repo: %v\n", err)
	}

	err = repo.Remove("image1.qcow2")
	if err != nil {
		t.Fatalf("Failed to remove image from populated image repo: %v\n", err)
	}

	err = repo.Remove("image3.raw")
	if err != nil {
		t.Fatalf("Failed to remove image from populated image repo: %v\n", err)
	}

	images, err := repo.FindAll()
	if err != nil {
		t.Fatalf("FindAll failed on empty repo\n")
	}

	if len(images) > 0 {
		t.Fatalf("Found images in emptied repo: %v\n", images)
	}
}
