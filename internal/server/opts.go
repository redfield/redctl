// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"time"
)

// RedctlServerOptions are the server's operational parameters
// Generally populated using UI/CLI flags.
type RedctlServerOptions struct {
	ServerURL          string
	TLSCertFile        string
	TLSKeyFile         string
	ServerRootPath     string
	DbPath             string
	ImagesPath         string
	GraphicsPath       string
	DataDir            string
	DomainStartTimeout time.Duration
}
