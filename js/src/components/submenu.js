// Copyright 2019 Assured Information Security, Inc.

import React from 'react';

const containerStyle = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: '80%',
    height: '75%',
    maxWidth: '80%',
    maxHeight: '75%',
    overflowX: 'hidden',
    overflowY: 'auto',
    backgroundColor: 'inherit'
};

const headerStyle = {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    height: '10%'
};

const style = {
    display: 'flex',
    margin: 'auto',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '90%'
};

const SubMenu = (props) => {
    let header = props.header ?
        (
            <div style={headerStyle}>
                <h2>{props.header}</h2>
            </div>
        )
        :
        null;

    return (
        <div style={containerStyle}>
            {header}
            <div style={props.style ? { ...style, ...props.style } : style}>
                {props.children ? props.children : (<div />)}
            </div>
        </div>
    );
};

export default SubMenu;
