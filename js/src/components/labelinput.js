// Copyright 2019 Assured Information Security, Inc.

import React from 'react';

import Button from './button';
import Row from './row';

const style = {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: '12%',
    minHeight: '12%',
    width: '95%',
    backgroundColor: 'inherit'
};

const LabelInput = (props) => {
    let onChange = (e) => {
        if (props.onChange) {
            if (e.target.files) {
                props.onChange(e.target.files[0].path);
                return;
            }

            props.onChange(e.target.value);
        }
    };

    let inputComponent = (
        <input
            type="text"
            onChange={onChange}
            placeholder={props.placeholder ? props.placeholder : undefined}
            value={props.value ? props.value : ''}
            style={props.inputStyle}
        />
    );

    if (props.type === 'dropdown') {
        let getOption = (opt) => {
            let display = String(opt);
            let value = display.toLowerCase();

            if (typeof opt === 'object' && opt.display && opt.value) {
                display = opt.display;
                value = opt.value;
            }

            return (<option value={value}>{display}</option>);
        };

        let options = props.options
            ?
            props.options.map((opt) => (getOption(opt)))
            :
            (<option>None</option>);

        inputComponent = (
            <select onChange={onChange} value={props.value ? props.value : undefined}>
                {
                    props.options
                        ?
                        props.options.map((opt) => (<option value={String(opt).toLowerCase()}>{String(opt)}</option>))
                        :
                        (<option>None</option>)
                }
            </select>
        );
    } else if (props.type === 'file') {
        let fileElement = undefined;
        let fileComponent = (
            <input type="file" ref={(input) => { fileElement = input; }}  onChange={onChange} style={{ display: 'none', width: '0%', height: '0%' }}/>
        );

        let onFileClick = () => { fileElement.click(); };

        inputComponent = (
            <Row style={{ justifyContent: 'flex-end', width: '70%', height: '' }}>
                <p style={{ marginRight: '5%' }}>{props.value ? props.value : '...'}</p>
                <Button onClick={onFileClick}>Choose File</Button>
                {fileComponent}
            </Row>
        );
    } else if (props.type === 'password') {
        inputComponent = (<input type="password" onChange={onChange} value={props.value ? props.value : undefined} style={props.inputStyle} />);
    }

    return (
        <div style={props.style ? { ...style, ...props.style } : style}>
            <h5>{props.label}</h5>
            {inputComponent}
        </div>
    );
};

export default LabelInput;
