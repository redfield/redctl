// Copyright 2019 Assured Information Security, Inc.

import React from 'react';

import Button from './button';

const style = {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '85%',
    height: '10%',
    backgroundColor: 'inherit'
};

const Navigator = (props) => (
    <div style={style}>
        <Button disabled={props.previousDisabled} onClick={props.onPrevious}>{props.prevLabel ? props.prevLabel : 'Previous'}</Button>
        <Button disabled={props.nextDisabled} onClick={props.onNext}>{props.nextLabel ? props.nextLabel : 'Next'}</Button>
    </div>
);

export default Navigator;
