// Copyright 2019 Assured Information Security, Inc.

import React, {Component} from 'react';

import Error from './error';
import Navigator from './navigator';
import SubMenu from './submenu';
import LabelInput from './labelinput';
import MultiInput from './multiinput';
import ValidatorInput from './validatorinput';

import Menu from '../utils/menu';
import {isValidVMName} from '../utils';

const style = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
};

const cpuMemoryStyle = {
    width: '8%'
};

const sizeStyle = {
    width: '10%'
};

const imageContainerStyle = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '90%',
    width: '19%',
    backgroundColor: 'grey',
    border: '1px solid white',
    borderRadius: '5px',
    userSelect: 'none'
};

const imageStyle = {
    display: 'block',
    width: '50px',
    height: '50px'
};

const previewStyle = {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    width: '95%',
    height: '30%',
    marginTop: '5%',
    alignItems: 'center'
};

// Empty state before the user navigates the wizard and after a reset (preserves configured values)
const emptyState = {
    error: undefined,
    selectedMenu: 0
};

var defaultConfig = {
    name: '',
    bridge: '',
    diskType: 'new'
};

// Represents the manual/graphical VM creation/configuration menus
export default class VMWizard extends Component {
    constructor(props) {
        super(props);

        this.setError = this.setError.bind(this);
        this.reset = this.reset.bind(this);
        this.recover = this.recover.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.handlePrevious = this.handlePrevious.bind(this);
        this.handleMainMenu = this.handleMainMenu.bind(this);
        this.setProperty = this.setProperty.bind(this);

        this.handleCreate = this.handleCreate.bind(this);
        this.handleImage = this.handleImage.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);

        this.getExistingDiskComponent = this.getExistingDiskComponent.bind(this);

        this.getBackends = this.getBackends.bind(this);
        this.getIcons = this.getIcons.bind(this);

        this.getIconPreview = this.getIconPreview.bind(this);

        this.setImage = this.setImage.bind(this);

        this.validateHardware = this.validateHardware.bind(this);
        this.validateDisks = this.validateDisks.bind(this);

        this.state = {
            ...emptyState,
            config: { ...defaultConfig }
        };
    }

    setError(err) {
        this.setState((state) => ({ ...state, error: err }));
    }

    getError() {
        return this.state.error ? this.state.error : this.props.error;
    }

    // Return to the first sub-menu (VM creation)
    reset() {
        defaultConfig = { ...this.state.config };

        this.setState((state) => ({
            ...state,
            ...emptyState
        }));

        this.props.reset();

        return false;
    }

    recover() {
        this.setError('');

        this.setState((state) => ({
            ...state,
            selectedMenu: state.selectedMenu - 1
        }));

        this.props.clearError();
    }

    // Determine whether user can navigate via the "Next" and "Previous" buttons
    canNavigate() {
        return (
            this.props.status !== 'creating'
            &&
            !this.props.error
        );
    }

    canNavigatePrevious() {
        return (
            !this.props.status
        );
    }

    // Return handlers that incorporate menu-specific actions and allow navigation
    // if those actions succeed
    handleNext(nextAction) {
        return (
            () => {
                this.setError('');

                if (nextAction()) {
                    this.setState((state) => ({ ...state, selectedMenu: state.selectedMenu + 1 }));
                }
            }
        );
    }

    handlePrevious(prevAction) {
        return (
            () => {
                this.setError('');

                if (prevAction()) {
                    this.setState((state) => ({ ...state, selectedMenu: state.selectedMenu - 1 }));
                }
            }
        );
    }

    handleMainMenu() {
        this.reset();

        this.props.toMainMenu();

        return false;
    }

    handleCreate() {
        if (!isValidVMName(this.state.config.name)) {
            this.setError('Please enter a valid VM name');

            return false;
        }

        let config = {
            name: this.state.config.name,
            desktopIcon: this.state.config.icon
        };

        if (!this.state.config.icon) {
            config.desktopIcon = this.props.icons[0];
        }

        this.props.handleCreate(config);

        return true;
    }

    handleImage() {
        if (!this.validateDisks()) {
            return false;
        }

        let config = {
            size: parseInt(this.state.config.diskSize),
            installMedia: this.state.config.installMedia
        };

        this.props.handleImage(config);

        return true;
    }

    handleUpdate() {
        let config = {
            vcpus: this.state.config.vcpus,
            memory: parseInt(this.state.config.memory),
            backend: this.state.config.backend,
            bridge: this.state.config.bridge
        };

        if (this.state.config.diskType == 'existing' && this.state.config.diskPath || !this.state.config.diskSize) {
            config.diskPath = this.state.config.diskImage ? this.state.config.diskImage : this.props.images[0];
        }

        if (!this.state.config.backend) {
            config.backend = this.props.backends[0];
        }

        this.props.handleUpdate(config);

        return false;
    }

    // Utilities that return setters for a given key within the VM configuration
    setProperty(key) {
        return (
            (value) => {
                this.setState((state) => {
                    let newState = { ...state };

                    newState.config[key] = value;

                    return newState;
                });
            }
        ).bind(this);
    }

    getExistingDiskComponent() {
        if (this.state.config.diskType === 'existing') {
            if (this.props.images && this.props.images.length) {
                return (
                    <LabelInput
                        label={'Disk Image:'}
                        type={'dropdown'}
                        options={this.props.images}
                        onChange={this.setProperty('diskImage')}
                        value={this.state.config.diskImage}
                    />
                );
            }
        }

        return null;
    }

    getBackends() {
        if (this.props.backends && this.props.backends.length) {
            return this.props.backends;
        }

        return ['None'];
    }

    getIcons() {
        if (this.props.icons && this.props.icons.length) {
            return this.props.icons;
        }

        return ['None'];
    }

    getIconPreview() {
        if (this.props.icons && this.props.icons.length) {
            let iconPath = this.props.icons
                .find((icon) => (icon === this.state.config.icon));

            if (!iconPath) {
                iconPath = this.props.icons[0];
            }

            return (
                <div style={previewStyle}>
                    <div style={imageContainerStyle}>
                        <img style={imageStyle} src={iconPath}/>
                    </div>
                </div>
            );
        }

        return null;
    }

    setImage() {
        // If an image has been created, set the configured path to default to the new image
        if (this.props.imagePath) {
            let config = { ...this.state.config };

            config.diskType = 'existing';

            config.diskImage = this.props.imagePath;

            this.setState((state) => ({ ...state, config, selectedMenu: state.selectedMenu - 2 }));
        }

        return false;
    }

    validateHardware() {
        if (
            !this.state.config.vcpus
            || isNaN(parseInt(this.state.config.vcpus))
        ) {
            this.setError('vCPUs must be an integer');
            return false;
        }

        if (
            !this.state.config.memory
            || isNaN(parseInt(this.state.config.memory))
        ) {
            this.setError('Memory must be an integer');
            return false;
        }

        return true;
    }

    validateDisks() {
        if (
            this.state.config.diskType === 'new'
            &&
            (!this.state.config.diskSize
            || isNaN(parseInt(this.state.config.diskSize))
            )
        ) {
            this.setError('Disk size must be an integer');
            return false;
        }

        return true;
    }

    render() {
        // VM configuration sub-menus
        let menus = [
            new Menu(
                (
                    <SubMenu header={'Basic Information'}>
                        <LabelInput
                            label={'Name:'}
                            onChange={this.setProperty('name')}
                            value={this.state.config.name}
                        />
                        <LabelInput
                            label={'Desktop Icon:'}
                            type={'dropdown'}
                            options={this.getIcons()}
                            onChange={this.setProperty('icon')}
                            value={this.state.config.icon}
                        />
                        {this.getIconPreview()}
                    </SubMenu>
                ),
                this.handleCreate,
                this.handleMainMenu
            ),
            new Menu(
                (
                    <SubMenu header={'Hardware Information'}>
                        <LabelInput
                            label={'vCPUs:'}
                            onChange={this.setProperty('vcpus')}
                            value={this.state.config.vcpus}
                            inputStyle={cpuMemoryStyle}
                        />
                        <LabelInput
                            label={'Memory (MB):'}
                            onChange={this.setProperty('memory')}
                            value={this.state.config.memory}
                            inputStyle={cpuMemoryStyle}
                        />
                        <LabelInput
                            label={'Disk Type:'}
                            type={'dropdown'}
                            options={['New', 'Existing']}
                            onChange={this.setProperty('diskType')}
                            value={this.state.config.diskType}
                        />
                        {this.getExistingDiskComponent()}
                    </SubMenu>
                ),
                this.validateHardware
            ),
            new Menu(
                (
                    <SubMenu header={'Network Information'}>
                        <LabelInput
                            label={'Network Backend:'}
                            type={'dropdown'}
                            options={this.getBackends()}
                            onChange={this.setProperty('backend')}
                            value={this.state.config.backend}
                        />
                        <LabelInput
                            label={'Bridge:'}
                            onChange={this.setProperty('bridge')}
                            value={this.state.config.bridge}
                        />
                    </SubMenu>
                ),
                this.handleUpdate,
                this.setImage
            )
        ];

        if (this.state.config.diskType === 'new') {
            menus.splice(2, 0,
                new Menu(
                    (
                        <SubMenu header={'Disk Information'}>
                            <LabelInput
                                label={'Size (GB):'}
                                onChange={this.setProperty('diskSize')}
                                value={this.state.config.diskSize}
                                inputStyle={sizeStyle}
                            />
                            <LabelInput
                                label={'Installation Media:'}
                                type={'file'}
                                onChange={this.setProperty('installMedia')}
                                value={this.state.config.installMedia}
                            />
                        </SubMenu>
                    ),
                    this.handleImage
                )
            );
        }

        // Determine menu to display
        let selectedMenu = menus[this.state.selectedMenu];

        let nextLabel = this.state.selectedMenu === menus.length - 1 ? 'Submit' : undefined;

        let prevLabel = this.state.selectedMenu === 0 ? 'Main Menu' : undefined;

        if (this.props.status === 'loading') {
            selectedMenu = new Menu(
                (
                    <SubMenu>
                        <h2>{this.props.statusMessage}</h2>
                        <div className={'lds-dual-ring'}/>
                    </SubMenu>
                )
            );
        } else if (this.props.status === 'success') {
            selectedMenu = new Menu(
                (
                    <SubMenu>
                        <h2>{this.props.statusMessage}</h2>
                        <h4>{'Click "Finish" to exit the application'}</h4>
                    </SubMenu>
                ),
                this.props.close
            );

            nextLabel = 'Finish';
        } else if (this.props.status === 'failed') {
            selectedMenu = new Menu(
                (
                    <SubMenu>
                        <h2>{'Failed to create VM: ' + this.state.config.name}</h2>
                        <h4>{'Click "Reset" to try again, or close the application to exit'}</h4>
                    </SubMenu>
                ),
                this.recover
            );

            nextLabel = 'Reset';
        }

        if (this.props.error) {
            selectedMenu = new Menu(
                (
                    <SubMenu>
                        <h2>Something went wrong</h2>
                    </SubMenu>
                ),
                undefined,
                this.recover
            );
        }

        return (
            <div className={'submenu'} style={style}>
                {selectedMenu.component}
                <Error text={this.getError()} />
                <Navigator
                    nextDisabled={!this.canNavigate()}
                    previousDisabled={!this.canNavigatePrevious()}
                    onNext={this.handleNext(selectedMenu.nextAction)}
                    onPrevious={this.handlePrevious(selectedMenu.prevAction)}
                    nextLabel={nextLabel}
                    prevLabel={prevLabel}
                />
            </div>
        );
    }
}
