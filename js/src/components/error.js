// Copyright 2019 Assured Information Security, Inc.

import React from 'react';

const style = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: '85%',
    height: '5%',
    color: 'red',
    backgroundColor: 'inherit'
};

const errorStyle = {
    textColor: 'red'
};

const Error = (props) => (
    <div style={style}>
        {props.text ? (<h5 style={errorStyle}>{props.text}</h5>) : null}
    </div>
);

export default Error;
