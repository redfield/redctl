// Copyright 2019 Assured Information Security, Inc.

const isValidVMName = (vmName) => (
    /^[a-zA-Z0-9-_]+$/
        .test(vmName)
);

export {
    isValidVMName
};
