// Copyright 2019 Assured Information Security, Inc.

export default class Menu {
    constructor(component, nextAction, prevAction) {
        if (nextAction && typeof nextAction !== 'function') {
            console.error('Menu next action must be a function');
        }

        if (prevAction && typeof prevAction !== 'function') {
            console.error('Menu previous action must be a function');
        }

        this.component = component;

        if (nextAction) {
            this.nextAction = nextAction;
        } else {
            this.nextAction = () => (true);
        }

        if (prevAction) {
            this.prevAction = prevAction;
        } else {
            this.prevAction = () => (true);
        }
    }
}
