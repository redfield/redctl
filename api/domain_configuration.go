// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"encoding"
	"fmt"
	"reflect"
	"strings"
)

// xlConfig represents an xl.cfg (https://xenbits.xen.org/docs/unstable/man/xl.cfg.5.html),
// and mirrors redfield DomainConfiguration in structure since
// gRPC does not allow custom tags, so XlConfig serves the purpose of providing tags.
type xlConfig struct {
	UUID         string `xl:"uuid"`
	Name         string `xl:"name"`
	Type         string `xl:"type"`
	VCPUs        string `xl:"vcpus"`
	Memory       string `xl:"memory"`
	Kernel       string `xl:"kernel"`
	RAMDisk      string `xl:"ramdisk"`
	CmdLine      string `xl:"cmdline"`
	DriverDomain string `xl:"driver_domain"`
	Boot         string `xl:"boot"`
	USB          string `xl:"usb"`
	USBDevice    string `xl:"usbdevice"`
	VGA          string `xl:"vga"`
	SDL          string `xl:"sdl"`
	OpenGL       string `xl:"opengl"`
	VNC          string `xl:"vnc"`
	SoundHW      string `xl:"soundhw"`

	Disks []*DomainDisk `xl:"disk"`

	Networks []*DomainNetwork `xl:"vif"`

	PCIDevices []*DomainPciDevice `xl:"pci"`

	USBControllers []*DomainUsbController `xl:"usbctrl"`

	DeviceModelArgs []string `xl:"device_model_args"`

	OnPoweroff  string `xl:"on_poweroff"`
	OnReboot    string `xl:"on_reboot"`
	OnWatchdog  string `xl:"on_watchdog"`
	OnCrash     string `xl:"on_crash"`
	OnSoftReset string `xl:"on_soft_reset"`
}

func domainConfigurationWithTags(dc DomainConfiguration) xlConfig {
	cfg := xlConfig{
		UUID:         dc.Uuid,
		Name:         dc.Name,
		Type:         dc.Type,
		VCPUs:        dc.Vcpus,
		Memory:       dc.Memory,
		Kernel:       dc.Kernel,
		RAMDisk:      dc.Ramdisk,
		CmdLine:      dc.Cmdline,
		DriverDomain: dc.DriverDomain,
		Boot:         dc.Boot,
		USB:          dc.Usb,
		USBDevice:    dc.Usbdevice,
		VGA:          dc.Vga,
		SDL:          dc.Sdl,
		OpenGL:       dc.Opengl,
		VNC:          dc.Vnc,
		SoundHW:      dc.Soundhw,

		Disks: dc.Disks,

		Networks: dc.Networks,

		PCIDevices: dc.Pcidevs,

		USBControllers: dc.Usbctrls,

		DeviceModelArgs: dc.DeviceModelArgs,

		// Use the string representation of the domain event
		OnPoweroff:  domainEventString[dc.OnPoweroff],
		OnReboot:    domainEventString[dc.OnReboot],
		OnWatchdog:  domainEventString[dc.OnWatchdog],
		OnCrash:     domainEventString[dc.OnCrash],
		OnSoftReset: domainEventString[dc.OnSoftReset],
	}

	return cfg
}

func formatArray(key string, spec []string) string {
	for i, v := range spec {
		spec[i] = fmt.Sprintf("\"%v\"", v)
	}

	return fmt.Sprintf("%v=[%v]", key, strings.Join(spec, ","))
}

func formatString(key, spec string) string {
	return fmt.Sprintf("%v=\"%v\"", key, spec)
}

// Serialize converts a DomainConfiguration into a byte stream in
// the textual format of an xl.cfg.
func (dc DomainConfiguration) Serialize() ([]byte, error) {
	var text []string

	// Convert to a type that has xl key tags we can control
	cfg := domainConfigurationWithTags(dc)
	t := reflect.TypeOf(cfg)

	for i := 0; i < t.NumField(); i++ {
		key := t.Field(i).Tag.Get("xl")
		rv := reflect.ValueOf(&cfg).Elem().Field(i)

		switch rv.Kind() {
		case reflect.Slice, reflect.Array:
			if rv.Len() == 0 {
				continue
			}
			var values []string

			// Check if rv is a slice of strings, or pointer to another type
			sk := rv.Index(0).Kind()

			if sk == reflect.String {
				values = rv.Interface().([]string)
			}

			if sk == reflect.Ptr {
				for j := 0; j < rv.Len(); j++ {
					// Assert that *rv implements encoding.TextMarshaler
					v := reflect.Indirect(rv.Index(j)).Interface().(encoding.TextMarshaler)
					b, err := v.MarshalText()
					if err != nil {
						return []byte{}, err
					}
					values = append(values, string(b))
				}
			}

			text = append(text, formatArray(key, values))

		case reflect.String:
			if rv.String() == "" {
				continue
			}
			text = append(text, formatString(key, rv.String()))
		}
	}

	return []byte(strings.Join(text, "\n")), nil
}
