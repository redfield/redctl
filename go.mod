module gitlab.com/redfield/redctl

require (
	github.com/Equanox/gotron v0.2.23
	github.com/gofrs/flock v0.7.1
	github.com/golang/protobuf v1.3.2
	github.com/google/uuid v1.1.1
	github.com/jcelliott/lumber v0.0.0-20160324203708-dd349441af25 // indirect
	github.com/nanobox-io/golang-scribble v0.0.0-20180621225840-336beac0a992
	github.com/otiai10/curr v1.0.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/rs/xid v1.2.1
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.7.1
	golang.org/x/crypto v0.0.0-20190605123033-f99c8df09eb5
	google.golang.org/grpc v1.21.1
)

go 1.11
